package com.example.servicepinger.polling;

import com.example.servicepinger.client.SimpleServiceClient;
import com.example.servicepinger.exception.ServiceUnavailableException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ScheduledPollingService {

    private final SimpleServiceClient simpleServiceClient;

    @Value("${simple-service.polling.max-retries}")
    private int maxRetries;

    @Value("${simple-service.polling.retry-time}")
    private int retryTime;

    private boolean shouldCheckSimpleService = true;


    @Scheduled(fixedRateString = "${simple-service.polling.time}")
    public void checkSimpleServiceScheduled() throws InterruptedException {
        if (!shouldCheckSimpleService) {
            return;
        }

        int retryCount = -1; //ignore first request

        while (retryCount < maxRetries) {
            try {
                int statusCode = getSimpleServiceInfo();

                if (statusCode != HttpStatus.OK.value()) {
                    retryCount++;
                } else {
                    log.info("External service is available");
                    return;
                }
            } catch (Exception e) {
                Thread.sleep(retryTime);
                retryCount++;
                log.info("External service is not available at the moment");
            }
        }

        if (retryCount == maxRetries) {
            shouldCheckSimpleService = false;
            throw new ServiceUnavailableException();
        }
    }

//    @Scheduled(fixedDelay = 30, timeUnit = TimeUnit.SECONDS)
//    public void changeShouldCheckSimpleServiceScheduled()  {
//        shouldCheckSimpleService = !shouldCheckSimpleService;
//    }

    private int getSimpleServiceInfo() {
        return simpleServiceClient.getInfo();
    }
}
