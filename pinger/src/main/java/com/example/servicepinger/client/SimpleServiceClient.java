package com.example.servicepinger.client;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
public class SimpleServiceClient {

    private static final String GET_INFO_ACTUATOR_URL = "actuator/info";
    private static final String SHUTDOWN_SERVICE_ACTUATOR_URL = "actuator/shutdown";

    private final RestTemplate restTemplate;
    @Value("${simple-service.endpoint.url}")
    private String actuatorEndpointUrl;

    public int getInfo() {
        return restTemplate.
                getForEntity(actuatorEndpointUrl + GET_INFO_ACTUATOR_URL, String.class).getStatusCodeValue();
    }
}
