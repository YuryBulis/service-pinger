package com.example.servicepinger.exception;

public class ServiceUnavailableException extends RuntimeException {

    public ServiceUnavailableException() {
        super("Service is not available at the moment. Please try again later");
    }
}
