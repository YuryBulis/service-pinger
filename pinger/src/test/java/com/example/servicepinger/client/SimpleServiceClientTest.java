package com.example.servicepinger.client;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class SimpleServiceClientTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private SimpleServiceClient simpleServiceClient;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void receive200WhenCallMethod() {
        when(restTemplate.getForEntity(anyString(), eq(String.class)))
                .thenReturn(new ResponseEntity<>("Response Body", HttpStatus.OK));
        int statusCodeValue = simpleServiceClient.getInfo();

        verify(restTemplate, times(1)).getForEntity(anyString(), eq(String.class));
        assertEquals(HttpStatus.OK.value(), statusCodeValue);
    }

    @Test
    void receive500WhenMethodFails() {
        when(restTemplate.getForEntity(anyString(), eq(String.class)))
                .thenReturn(new ResponseEntity<>("Error Body", HttpStatus.INTERNAL_SERVER_ERROR));
        int statusCodeValue = simpleServiceClient.getInfo();

        verify(restTemplate, times(1)).getForEntity(anyString(), eq(String.class));
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), statusCodeValue);
    }

}